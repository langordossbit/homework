#include <iostream>
#include <new>

class animal
{	
public:
	animal() {}
	virtual int Voice()
	{
		std::cout << "Animal" << std::endl;
		return 0;
	}
};

class dog : public animal
{
public:
	int Voice() override
	{
		std::cout << "Woof" << std::endl;
		return 0;
	}
};

class cat : public animal
{
public:
	int Voice() override
	{
		std::cout << "Meow" << std::endl;
		return 0;
	}
};

class bird : public animal
{
public:
	int Voice() override
	{
		std::cout << "Cherek,cherek" << std::endl;
		return 0;
	}
};
int main()
{
	animal** array = new animal * [3];

	dog* a = new dog;
	cat* b = new cat;
	bird* c = new bird;

	array[0] = a;
	array[1] = b;
	array[2] = c;

	for (int x = 0; x < 3; x++)
	{
		array[x]->Voice();
	}

	
}